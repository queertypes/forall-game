{-# LANGUAGE OverloadedStrings #-}
module Forall.Loop where

import Control.Concurrent (threadDelay)
import Control.Monad.IO.Class
import Data.Foldable
import Data.Monoid
import Data.Word
import Foreign.C.Types
import Linear
import qualified SDL
import qualified SDL.Input.Keyboard as SDL


type Color = V4 Word8

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

white, black, red, green, blue :: Color
white = V4 maxBound maxBound maxBound maxBound
black = V4 minBound minBound minBound maxBound
red   = V4 maxBound minBound minBound maxBound
green = V4 minBound maxBound minBound maxBound
blue  = V4 minBound minBound maxBound maxBound

fillSurace :: MonadIO m => SDL.Surface -> Color -> m ()
fillSurace surface color = SDL.surfaceFillRect surface Nothing color

updateSurface :: MonadIO m => SDL.Window -> m ()
updateSurface = SDL.updateWindowSurface

data Action
  = Quit
  | UpdateColor !Color
  | Ignore
  | Log !SDL.Keysym

keySymbol :: SDL.KeyboardEventData -> SDL.Keysym
keySymbol = SDL.keyboardEventKeysym

isQuit :: SDL.Keysym -> Bool
isQuit k = (SDL.unwrapKeycode $ SDL.keysymKeycode k) == 113

keyToAction :: SDL.Keysym -> Action
keyToAction s =
  case (SDL.unwrapKeycode . SDL.keysymKeycode) s of
    113 -> Quit
    98  -> UpdateColor blue
    114 -> UpdateColor red
    103 -> UpdateColor green
    119 -> UpdateColor white
    _   -> Log s

eventToAction :: SDL.Event -> Action
eventToAction ev =
  case SDL.eventPayload ev of
    (SDL.KeyboardEvent x) -> keyToAction (keySymbol x)
    _                 -> Ignore

-- | Convert SDL events to game actions
actions :: [SDL.Event] -> [Action]
actions = reverse . foldl' (\acc x -> eventToAction x : acc) []

-- | If there are actions, process those, else poll for events
loop :: SDL.Window -> SDL.Surface -> [Action] -> IO ()
loop window surface (a:as) =
  case a of
    Quit -> return ()
    (UpdateColor c) -> do
      fillSurace surface c
      updateSurface window
      loop window surface as
    Ignore -> loop window surface as
    (Log k) -> putStrLn (show k) >> loop window surface as
loop window surface [] = do
  evs <- SDL.pollEvents
  loop window surface (actions evs)

run :: IO ()
run = do
  SDL.initializeAll
  window <- SDL.createWindow "Forall" SDL.defaultWindow { SDL.windowInitialSize = V2 screenWidth screenHeight }
  SDL.showWindow window
  screenSurface <- SDL.getWindowSurface window
  loop window screenSurface []
  SDL.destroyWindow window
  SDL.quit
